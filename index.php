<?php 
	if( file_exists( 'db.php' ) ){
		include('db.php');
	}
	if( file_exists( 'functions.php' ) ){
		include('functions.php');
		$instant = new HelperClass();
		$instant->insertEvent();
	}
?>
<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Zoom Template</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/styles.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script type="text/javascript">
			function get_marker(){
				var markers = [<?php $datalist = $instant->dataFetch();$comma = "";while( $rows = mysql_fetch_assoc( $datalist ) ) : ?><?php echo $comma; ?>['<?php echo $rows['event_title']; ?>', <?php echo $rows['event_lat'] ?>,<?php echo $rows['event_long'] ?>]<?php  $comma = ",";	endwhile; ?>];
        		if( markers.length == 0 ){
        			 markers = [['London Eye, London', 51.503454,-0.119562]];
        		}
   				return markers;
			}
			function get_info(){
				 // Info Window Content
			    var infoWindowContent = [
				    <?php $datalist = $instant->dataFetch();
				    $comma = "";
					while( $rows = mysql_fetch_assoc( $datalist ) ) : ?>
				        <?php echo $comma; ?>['<div class="info_content">' +
				        '<h3><?php echo $rows['event_title']; ?></h3>' +
				        '<p><?php echo $rows['event_address']; ?></p>' +
				        '<p><?php echo $rows['event_desc']; ?></p>' +'</div>']
				    <?php  $comma = ",";
				    endwhile; ?>
				    ];
			    if( infoWindowContent.length == 0 ){
        			 infoWindowContent =  [['<div class="info_content">' +
				        '<h3>Event 1</h3>' +
				        '<p>Address 1</p>' +'</div>']];
        		}
        		return infoWindowContent;
			}
		</script>
	</head>
	<body>
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo $home_url; ?>">Zoom Template</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a data-toggle="modal" href='#modal-id-1'>Login</a></li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div>
		</nav>
			  <!-- /Google Map BOC -->
		<section class="location-map">
			<div id="map">
			  <div id="googlemaps" class="google-map">
			    <div class="gmap_marker"></div>
			  </div>
			</div>
		</section>
		<!-- /Google Map EOC -->
		<section class="search-cat-section">
			<div class="container">
				<div class="row">
					<div class="col-md-7 text-right common-form text-right">
						<input type="text" id="lib" class="form-control ui-widget ui-widget-content" placeholder="Search Form">
					</div>
					<div class="col-md-5 text-center">
						<div data-example-id="buttons-checkbox" class="bs-example btn-toggle">
							<div data-toggle="buttons" class="btn-group"> 
								<label class="btn btn-primary">
									<input type="checkbox" autocomplete="off" checked="checked">Cat1</label>
									<label class="btn btn-primary active">
									<input type="checkbox" autocomplete="off">Cat2</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="modal fade" id="modal-id-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Login Form</h4>
					</div>
					<div class="modal-body">
						<div class="common-form">
						  <div class="bs-example" data-example-id="simple-horizontal-form">
						    <form class="form-horizontal" method="post" action="<?php echo $home_url ?>admin.php">
						      <div class="form-group">
						        <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
						        <div class="col-sm-10">
						          <input type="text" name="user" class="form-control" id="inputEmail3" placeholder="User">
						        </div>
						      </div>
						      <div class="form-group">
						        <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
						        <div class="col-sm-10">
						          <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
						        </div>
						      </div>
						      <div class="form-group">
						        <div class="col-sm-offset-2 col-sm-10">
						          <button type="submit" class="btn btn-default">Sign in</button>
						        </div>
						      </div>
						    </form>
						  </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<section class="area-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="cat-title">Cat 1</div>
						<div class="row">
							<?php $datalist = $instant->dataFetch(1);
							while( $rows = mysql_fetch_assoc( $datalist ) ) : ?>
							<div class="col-md-6" itemscope itemtype="http://schema.org/Event">
								<meta itemprop="name" content="<?php echo $rows['event_title']; ?>"/>
								<meta itemprop="startDate" content="<?php echo $rows['event_start']; ?>"/>
								<meta itemprop="endDate" content="<?php echo $rows['event_end']; ?>"/>
								<div class="wrapplace">
									<div class="head-detail">
										<label class="pull-left">
											<span class="date-start"><?php echo $rows['event_start']; ?></span>
											<span class="date-end"><?php echo $rows['event_end']; ?></span>
										</label>
										<label class="pull-right"><?php echo $instant->fetchCategory( $rows['event_cat'] ); ?></label>
									</div>
									<div class="detail2"><h4><?php echo $rows['event_title']; ?></h4></div>
									<p><?php echo $instant->fetchCountry( $rows['event_country'] ); ?></p>
									<br>
									<div class="siteurl"><a href="<?php echo $rows['event_url']; ?>"><?php echo $rows['event_url']; ?></a></div>
									<button class="button-url">Show in Map</button>
								</div>
							</div>
							<?php endwhile; ?>
						</div>
					</div>
					<div class="col-md-4">
						<div class="cat-title">Cat 2</div>
						<?php $datalist = $instant->dataFetch(2);
						while( $rows = mysql_fetch_assoc( $datalist ) ) : ?>
						<div class="wrapplace">
							<div class="head-detail">
								<label class="pull-left">
									<span class="date-start"><?php echo $rows['event_start']; ?></span>
									<span class="date-end"><?php echo $rows['event_end']; ?></span>
								</label>
								<label class="pull-right"><?php echo $instant->fetchCategory( $rows['event_cat'] ); ?></label>
							</div>
							<div class="detail2"><h4><?php echo $rows['event_title']; ?></h4></div>
							<p><?php echo $instant->fetchCountry( $rows['event_country'] ); ?></p>
							<br>
							<div class="siteurl"><a href="<?php echo $rows['event_url']; ?>"><?php echo $rows['event_url']; ?></a></div>
							<button class="button-url">Show in Map</button>
						</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		</section>
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/jquery-ui.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script type='text/javascript' src='assets/js/jquery-map.js'></script>
		<script type='text/javascript' src="assets/js/script.js"></script>
	</body>
</html>