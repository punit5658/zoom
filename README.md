# README #
Project is developed for test purpose.

Current project is contain phase I development as per provided details.

Access Admin panel: 

siteurl/admin.php

User : admin

Password: secretpassword

### How to setup project? ###
* Database setup
  - db.php 
    * Add Database name
    * Add Database User
    * Add Database Password
    * Add Siteurl
    * Add Admin URL

Create Database Using : 

CREATE DATABASE `event`;
--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `event_title` varchar(55) NOT NULL,
  `event_cat` int(2) NOT NULL,
  `event_desc` text NOT NULL,
  `event_address` text NOT NULL,
  `event_zipcode` int(6) NOT NULL,
  `event_country` varchar(50) NOT NULL,
  `event_lat` float NOT NULL,
  `event_long` float NOT NULL,
  `event_start` date NOT NULL,
  `event_end` date NOT NULL,
  `event_url` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;