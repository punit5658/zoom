    var map;
            var myCenter=new google.maps.LatLng(53, -1.33);
      var marker=new google.maps.Marker({
          position:myCenter
      });

      var mapProp = {
        center:myCenter,
        zoom: 10,
        mapTypeId:google.maps.MapTypeId.ROADMAP
      };
      function initialize() {
        map=new google.maps.Map(document.getElementById("map-canvas"),mapProp);
        marker.setMap(map);

        google.maps.event.addListener(marker, 'click', function() {
          infowindow.setContent(contentString);
          infowindow.open(map, marker);
        });
      };

      google.maps.event.addListener(mapProp, 'click', function(event) {
        document.getElementById("inputlong").value = event.latLng.lng().toFixed(6);
        document.getElementById("inputlat").value = event.latLng.lat().toFixed(6);
        marker.setPosition(event.latLng);
        if_gmap_updateInfoWindow();
      });
      google.maps.event.addDomListener(window, 'load', initialize);

      google.maps.event.addDomListener(window, "resize", resizingMap());

      $('#myMapModal').on('show.bs.modal', function() {
         //Must wait until the render of the modal appear, thats why we use the resizeMap and NOT resizingMap!! ;-)
         resizeMap();
      })

      function resizeMap() {
         if(typeof map =="undefined") return;
         setTimeout( function(){resizingMap();} , 400);
      }

      function resizingMap() {
         if(typeof map =="undefined") return;
         var center = map.getCenter();
         google.maps.event.trigger(map, "resize");
         map.setCenter(center);
      }
      infoWindow = new google.maps.InfoWindow;
      function if_gmap_updateInfoWindow()
      {
        infoWindow.setContent("Longitude: "+ marker.getPosition().lng().toFixed(6)+"<br>"+"Latitude: "+ marker.getPosition().lat().toFixed(6));
      }
      jQuery(document).ready(function($) {
        $('#inputzip1').on('keypress',function(e){

          if( e.keyCode == 8 ){
            return true;
          }

            if( $(this).val().length > 5 ){
              return false;
            }
        });
      });