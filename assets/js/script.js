/* Google MAP JS BOF
===================================================== */
jQuery(document).ready(function($) {
 
 // Map Markers
 var mapMarkers = [{
  address: "Hidcote Terrace, Wollert VIC 3750, Australia",
  html: 
   "<strong>21, Hidcote Terrace,<br></strong><strong>Wollert VIC 3750 </strong><br><strong>Australia</strong>", 
  icon: {
   image: "assets/images/pin.png", // Map Icon Goes Here
   iconsize: [60, 79],
   iconanchor: [60, 79]
  },
  popup: true
 }];

 // Map Initial Location
 var initLatitude = -37.619406; // Latitude
 var initLongitude = 144.997095; // Longintute

 // Map Extended Settings
 var mapSettings = {
  controls: {
   panControl: true,
   zoomControl: true,
   mapTypeControl: true,
   scaleControl: true,
   streetViewControl: true,
   overviewMapControl: true
  },
  scrollwheel: false,
  markers: mapMarkers,
  latitude: initLatitude,
  longitude: initLongitude,
  
  zoom: 5 // Map Zooming
 };

// var map = jQuery("#googlemaps").gMap(mapSettings);


 // Map Center At
 var mapCenterAt = function(options, e) {
    e.preventDefault();
    if( $('#googlemaps').length )
    $("#googlemaps").gMap("centerAt", options);
 }
});

/* Multiple Marker supporter */

jQuery(function($) {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "//maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
    document.body.appendChild(script);
});

function initialize() {
    get_marker();
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap',
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("googlemaps"), mapOptions);
    map.setTilt(45);
        
    // Multiple Markers
    var markers = get_marker();
                        
    // Info Window Content
    var infoWindowContent = get_info();
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(4);
        google.maps.event.removeListener(boundsListener);
    });
    
}


