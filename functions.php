<?php 
	if( isset( $home_url )){
		/**
		* HelperClass
		*/
		class HelperClass
		{
			public function insertEvent(){
				if( !isset( $_POST['event_title'] ) )
					return true;
				
				$event_title   = isset( $_POST['event_title'] ) ? (string)$_POST['event_title'] : "";
				$event_cat     = isset( $_POST['event_cat'] ) ? (int)$_POST['event_cat']: "";
				$event_desc    = isset( $_POST['event_desc'] ) ? (string)$_POST['event_desc']: "";
				$event_address = isset( $_POST['event_add'] ) ? (string)$_POST['event_add']: "";
				$event_zipcode = isset( $_POST['event_zipcode'] ) ? (int)$_POST['event_zipcode'] : "";
				$event_country = isset( $_POST['event_country'] ) ? (string)$_POST['event_country'] : "";
				$event_lat     = isset( $_POST['event_lat'] )? (float)$_POST['event_lat']: "";
				$event_long    = isset( $_POST['event_long'] )? (float)$_POST['event_long']: "";
				$event_start   = isset( $_POST['event_start'] )? (string)$_POST['event_start']: "";
				$event_end     = isset( $_POST['event_end'] )? (string)$_POST['event_end']: "";
				$event_url     = isset( $_POST['event_url'] )? (string)$_POST['event_url']: "";
				$event_start   = date( 'Y-m-d', strtotime( $event_start ));
				$event_end     = date( 'Y-m-d', strtotime( $event_end ));
						$sql = "INSERT INTO  `zoom`.`event` (
									`ID` ,
									`event_title` ,
									`event_cat` ,
									`event_desc` ,
									`event_address` ,
									`event_zipcode` ,
									`event_country` ,
									`event_lat` ,
									`event_long` ,
									`event_start` ,
									`event_end`,
									`event_url`
									)
									VALUES (
									NULL , '$event_title'  , $event_cat, '$event_address', '$event_desc', $event_zipcode, '$event_country', $event_lat, $event_long, '$event_start', '$event_end','$$event_url'
										);";
				
				mysql_query( $sql );
			}

			public function editEvent( $e_id ){

			}

			public function dataFetch( $where = ''){
				if( $where != "" ){
					$where = 'where event_cat='. $where;
				}
				$data = mysql_query( "select * from `zoom`.`event` $where;" );
				return $data;
			}

			public function fetchCategory( $category_key = NULL ){
				$category = array(
								"1" => "Category 1",
								"2" => "Category 2",
							);
				if( $category_key != NULL ){
					return $category_name = isset( $category[ $category_key ] )? $category[ $category_key ] : "";
				}
				return $category;
			}

			public function fetchCountry( $country_key = NULL ){

				$country = array( 	 
						 "ISO 3166-2:AF" => "Afghanistan",
						 "ISO 3166-2:AX" => "Åland Islands",
						 "ISO 3166-2:AL" => "Albania",
						 "ISO 3166-2:DZ" => "Algeria",
						 "ISO 3166-2:AS" => "American Samoa",
						 "ISO 3166-2:AD" => "Andorra",
						 "ISO 3166-2:AO" => "Angola",
						 "ISO 3166-2:AI" => "Anguilla",
						 "ISO 3166-2:AQ" => "Antarctica",
						 "ISO 3166-2:AG" => "Antigua and Barbuda",
						 "ISO 3166-2:AR" => "Argentina",
						 "ISO 3166-2:AM" => "Armenia",
						 "ISO 3166-2:AW" => "Aruba",
						 "ISO 3166-2:AU" => "Australia",
						 "ISO 3166-2:AT" => "Austria",
						 "ISO 3166-2:AZ" => "Azerbaijan",
						 "ISO 3166-2:BS" => "Bahamas",
						 "ISO 3166-2:BH" => "Bahrain",
						 "ISO 3166-2:BD" => "Bangladesh",
						 "ISO 3166-2:BB" => "Barbados",
						 "ISO 3166-2:BY" => "Belarus",
						 "ISO 3166-2:BE" => "Belgium",
						 "ISO 3166-2:BZ" => "Belize",
						 "ISO 3166-2:BJ" => "Benin",
						 "ISO 3166-2:BM" => "Bermuda",
						 "ISO 3166-2:BT" => "Bhutan",
						 "ISO 3166-2:BO" => "Bolivia, Plurinational State of",
						 "ISO 3166-2:BQ" => "Bonaire, Sint Eustatius and Saba",
						 "ISO 3166-2:BA" => "Bosnia and Herzegovina",
						 "ISO 3166-2:BW" => "Botswana",
						 "ISO 3166-2:BV" => "Bouvet Island",
						 "ISO 3166-2:BR" => "Brazil",
						 "ISO 3166-2:IO" => "British Indian Ocean Territory",
						 "ISO 3166-2:BN" => "Brunei Darussalam",
						 "ISO 3166-2:BG" => "Bulgaria",
						 "ISO 3166-2:BF" => "Burkina Faso",
						 "ISO 3166-2:BI" => "Burundi",
						 "ISO 3166-2:KH" => "Cambodia",
						 "ISO 3166-2:CM" => "Cameroon",
						 "ISO 3166-2:CA" => "Canada",
						 "ISO 3166-2:CV" => "Cape Verde",
						 "ISO 3166-2:KY" => "Cayman Islands",
						 "ISO 3166-2:CF" => "Central African Republic",
						 "ISO 3166-2:TD" => "Chad",
						 "ISO 3166-2:CL" => "Chile",
						 "ISO 3166-2:CN" => "China",
						 "ISO 3166-2:CX" => "Christmas Island",
						 "ISO 3166-2:CC" => "Cocos (Keeling) Islands",
						 "ISO 3166-2:CO" => "Colombia",
						 "ISO 3166-2:KM" => "Comoros",
						 "ISO 3166-2:CG" => "Congo",
						 "ISO 3166-2:CD" => "Congo, the Democratic Republic of the",
						 "ISO 3166-2:CK" => "Cook Islands",
						 "ISO 3166-2:CR" => "Costa Rica",
						 "ISO 3166-2:CI" => "Côte dIvoire",
						 "ISO 3166-2:HR" => "Croatia",
						 "ISO 3166-2:CU" => "Cuba",
						 "ISO 3166-2:CW" => "Curaçao",
						 "ISO 3166-2:CY" => "Cyprus",
						 "ISO 3166-2:CZ" => "Czech Republic",
						 "ISO 3166-2:DK" => "Denmark",
						 "ISO 3166-2:DJ" => "Djibouti",
						 "ISO 3166-2:DM" => "Dominica",
						 "ISO 3166-2:DO" => "Dominican Republic",
						 "ISO 3166-2:EC" => "Ecuador",
						 "ISO 3166-2:EG" => "Egypt",
						 "ISO 3166-2:SV" => "El Salvador",
						 "ISO 3166-2:GQ" => "Equatorial Guinea",
						 "ISO 3166-2:ER" => "Eritrea",
						 "ISO 3166-2:EE" => "Estonia",
						 "ISO 3166-2:ET" => "Ethiopia",
						 "ISO 3166-2:FK" => "Falkland Islands (Malvinas)",
						 "ISO 3166-2:FO" => "Faroe Islands",
						 "ISO 3166-2:FJ" => "Fiji",
						 "ISO 3166-2:FI" => "Finland",
						 "ISO 3166-2:FR" => "France",
						 "ISO 3166-2:GF" => "French Guiana",
						 "ISO 3166-2:PF" => "French Polynesia",
						 "ISO 3166-2:TF" => "French Southern Territories",
						 "ISO 3166-2:GA" => "Gabon",
						 "ISO 3166-2:GM" => "Gambia",
						 "ISO 3166-2:GE" => "Georgia",
						 "ISO 3166-2:DE" => "Germany",
						 "ISO 3166-2:GH" => "Ghana",
						 "ISO 3166-2:GI" => "Gibraltar",
						 "ISO 3166-2:GR" => "Greece",
						 "ISO 3166-2:GL" => "Greenland",
						 "ISO 3166-2:GD" => "Grenada",
						 "ISO 3166-2:GP" => "Guadeloupe",
						 "ISO 3166-2:GU" => "Guam",
						 "ISO 3166-2:GT" => "Guatemala",
						 "ISO 3166-2:GG" => "Guernsey",
						 "ISO 3166-2:GN" => "Guinea",
						 "ISO 3166-2:GW" => "Guinea-Bissau",
						 "ISO 3166-2:GY" => "Guyana",
						 "ISO 3166-2:HT" => "Haiti",
						 "ISO 3166-2:HM" => "Heard Island and McDonald Islands",
						 "ISO 3166-2:VA" => "Holy See (Vatican City State)",
						 "ISO 3166-2:HN" => "Honduras",
						 "ISO 3166-2:HK" => "Hong Kong",
						 "ISO 3166-2:HU" => "Hungary",
						 "ISO 3166-2:IS" => "Iceland",
						 "ISO 3166-2:IN" => "India",
						 "ISO 3166-2:ID" => "Indonesia",
						 "ISO 3166-2:IR" => "Iran, Islamic Republic of",
						 "ISO 3166-2:IQ" => "Iraq",
						 "ISO 3166-2:IE" => "Ireland",
						 "ISO 3166-2:IM" => "Isle of Man",
						 "ISO 3166-2:IL" => "Israel",
						 "ISO 3166-2:IT" => "Italy",
						 "ISO 3166-2:JM" => "Jamaica",
						 "ISO 3166-2:JP" => "Japan",
						 "ISO 3166-2:JE" => "Jersey",
						 "ISO 3166-2:JO" => "Jordan",
						 "ISO 3166-2:KZ" => "Kazakhstan",
						 "ISO 3166-2:KE" => "Kenya",
						 "ISO 3166-2:KI" => "Kiribati",
						 "ISO 3166-2:KP" => "Korea, Democratic Peoples Republic of",
						 "ISO 3166-2:KR" => "Korea, Republic of",
						 "ISO 3166-2:KW" => "Kuwait",
						 "ISO 3166-2:KG" => "Kyrgyzstan",
						 "ISO 3166-2:LA" => "Lao Peoples Democratic Republic",
						 "ISO 3166-2:LV" => "Latvia",
						 "ISO 3166-2:LB" => "Lebanon",
						 "ISO 3166-2:LS" => "Lesotho",
						 "ISO 3166-2:LR" => "Liberia",
						 "ISO 3166-2:LY" => "Libya",
						 "ISO 3166-2:LI" => "Liechtenstein",
						 "ISO 3166-2:LT" => "Lithuania",
						 "ISO 3166-2:LU" => "Luxembourg",
						 "ISO 3166-2:MO" => "Macao",
						 "ISO 3166-2:MK" => "Macedonia, the former Yugoslav Republic of",
						 "ISO 3166-2:MG" => "Madagascar",
						 "ISO 3166-2:MW" => "Malawi",
						 "ISO 3166-2:MY" => "Malaysia",
						 "ISO 3166-2:MV" => "Maldives",
						 "ISO 3166-2:ML" => "Mali",
						 "ISO 3166-2:MT" => "Malta",
						 "ISO 3166-2:MH" => "Marshall Islands",
						 "ISO 3166-2:MQ" => "Martinique",
						 "ISO 3166-2:MR" => "Mauritania",
						 "ISO 3166-2:MU" => "Mauritius",
						 "ISO 3166-2:YT" => "Mayotte",
						 "ISO 3166-2:MX" => "Mexico",
						 "ISO 3166-2:FM" => "Micronesia, Federated States of",
						 "ISO 3166-2:MD" => "Moldova, Republic of",
						 "ISO 3166-2:MC" => "Monaco",
						 "ISO 3166-2:MN" => "Mongolia",
						 "ISO 3166-2:ME" => "Montenegro",
						 "ISO 3166-2:MS" => "Montserrat",
						 "ISO 3166-2:MA" => "Morocco",
						 "ISO 3166-2:MZ" => "Mozambique",
						 "ISO 3166-2:MM" => "Myanmar",
						 "ISO 3166-2:NA" => "Namibia",
						 "ISO 3166-2:NR" => "Nauru",
						 "ISO 3166-2:NP" => "Nepal",
						 "ISO 3166-2:NL" => "Netherlands",
						 "ISO 3166-2:NC" => "New Caledonia",
						 "ISO 3166-2:NZ" => "New Zealand",
						 "ISO 3166-2:NI" => "Nicaragua",
						 "ISO 3166-2:NE" => "Niger",
						 "ISO 3166-2:NG" => "Nigeria",
						 "ISO 3166-2:NU" => "Niue",
						 "ISO 3166-2:NF" => "Norfolk Island",
						 "ISO 3166-2:MP" => "Northern Mariana Islands",
						 "ISO 3166-2:NO" => "Norway",
						 "ISO 3166-2:OM" => "Oman",
						 "ISO 3166-2:PK" => "Pakistan",
						 "ISO 3166-2:PW" => "Palau",
						 "ISO 3166-2:PS" => "Palestinian Territory, Occupied",
						 "ISO 3166-2:PA" => "Panama",
						 "ISO 3166-2:PG" => "Papua New Guinea",
						 "ISO 3166-2:PY" => "Paraguay",
						 "ISO 3166-2:PE" => "Peru",
						 "ISO 3166-2:PH" => "Philippines",
						 "ISO 3166-2:PN" => "Pitcairn",
						 "ISO 3166-2:PL" => "Poland",
						 "ISO 3166-2:PT" => "Portugal",
						 "ISO 3166-2:PR" => "Puerto Rico",
						 "ISO 3166-2:QA" => "Qatar",
						 "ISO 3166-2:RE" => "Réunion",
						 "ISO 3166-2:RO" => "Romania",
						 "ISO 3166-2:RU" => "Russian Federation",
						 "ISO 3166-2:RW" => "Rwanda",
						 "ISO 3166-2:BL" => "Saint Barthélemy",
						 "ISO 3166-2:SH" => "Saint Helena, Ascension and Tristan da Cunha",
						 "ISO 3166-2:KN" => "Saint Kitts and Nevis",
						 "ISO 3166-2:LC" => "Saint Lucia",
						 "ISO 3166-2:MF" => "Saint Martin (French part)",
						 "ISO 3166-2:PM" => "Saint Pierre and Miquelon",
						 "ISO 3166-2:VC" => "Saint Vincent and the Grenadines",
						 "ISO 3166-2:WS" => "Samoa",
						 "ISO 3166-2:SM" => "San Marino",
						 "ISO 3166-2:ST" => "Sao Tome and Principe",
						 "ISO 3166-2:SA" => "Saudi Arabia",
						 "ISO 3166-2:SN" => "Senegal",
						 "ISO 3166-2:RS" => "Serbia",
						 "ISO 3166-2:SC" => "Seychelles",
						 "ISO 3166-2:SL" => "Sierra Leone",
						 "ISO 3166-2:SG" => "Singapore",
						 "ISO 3166-2:SX" => "Sint Maarten (Dutch part)",
						 "ISO 3166-2:SK" => "Slovakia",
						 "ISO 3166-2:SI" => "Slovenia",
						 "ISO 3166-2:SB" => "Solomon Islands",
						 "ISO 3166-2:SO" => "Somalia",
						 "ISO 3166-2:ZA" => "South Africa",
						 "ISO 3166-2:GS" => "South Georgia and the South Sandwich Islands",
						 "ISO 3166-2:SS" => "South Sudan",
						 "ISO 3166-2:ES" => "Spain",
						 "ISO 3166-2:LK" => "Sri Lanka",
						 "ISO 3166-2:SD" => "Sudan",
						 "ISO 3166-2:SR" => "Suriname",
						 "ISO 3166-2:SJ" => "Svalbard and Jan Mayen",
						 "ISO 3166-2:SZ" => "Swaziland",
						 "ISO 3166-2:SE" => "Sweden",
						 "ISO 3166-2:CH" => "Switzerland",
						 "ISO 3166-2:SY" => "Syrian Arab Republic",
						 "ISO 3166-2:TW" => "Taiwan, Province of China",
						 "ISO 3166-2:TJ" => "Tajikistan",
						 "ISO 3166-2:TZ" => "Tanzania, United Republic of",
						 "ISO 3166-2:TH" => "Thailand",
						 "ISO 3166-2:TL" => "Timor-Leste",
						 "ISO 3166-2:TG" => "Togo",
						 "ISO 3166-2:TK" => "Tokelau",
						 "ISO 3166-2:TO" => "Tonga",
						 "ISO 3166-2:TT" => "Trinidad and Tobago",
						 "ISO 3166-2:TN" => "Tunisia",
						 "ISO 3166-2:TR" => "Turkey",
						 "ISO 3166-2:TM" => "Turkmenistan",
						 "ISO 3166-2:TC" => "Turks and Caicos Islands",
						 "ISO 3166-2:TV" => "Tuvalu",
						 "ISO 3166-2:UG" => "Uganda",
						 "ISO 3166-2:UA" => "Ukraine",
						 "ISO 3166-2:AE" => "United Arab Emirates",
						 "ISO 3166-2:GB" => "United Kingdom",
						 "ISO 3166-2:US" => "United States",
						 "ISO 3166-2:UM" => "United States Minor Outlying Islands",
						 "ISO 3166-2:UY" => "Uruguay",
						 "ISO 3166-2:UZ" => "Uzbekistan",
						 "ISO 3166-2:VU" => "Vanuatu",
						 "ISO 3166-2:VE" => "Venezuela, Bolivarian Republic of",
						 "ISO 3166-2:VN" => "Viet Nam",
						 "ISO 3166-2:VG" => "Virgin Islands, British",
						 "ISO 3166-2:VI" => "Virgin Islands, U.S.",
						 "ISO 3166-2:WF" => "Wallis and Futuna",
						 "ISO 3166-2:EH" => "Western Sahara",
						 "ISO 3166-2:YE" => "Yemen",
						 "ISO 3166-2:ZM" => "Zambia",
						 "ISO 3166-2:ZW" => "Zimbabwe" );

				if( $country_key != NULL ){
					return $country_name = isset( $country[ $country_key ] )? $country[ $country_key ] : "";
				}
				return $country;
			}
		}
	}
?>