<?php 
	if( file_exists( 'db.php' ) ){
		include('db.php');		
	}
	session_start(); 
	$status = isset( $_GET['status'] )? $_GET['status']: "";
	if( isset( $_POST['user']) && ( $_POST['user'] == 'admin' ) && isset($_POST['password']) && ($_POST['password'] == 'secretpassword')){
		$_SESSION['user_logged_in'] = 1;
	}	
	if( !empty( $status ) && $status == "loggedout" ){
		if( isset( $_SESSION['user_logged_in'] ) ){
			unset( $_SESSION['user_logged_in'] );		
		}
		header('Location: '.$home_url);
	}
	if( !isset( $_SESSION['user_logged_in'] ) && $_SESSION['user_logged_in'] != 1 ){
		header('Location: '.$home_url);
	}
	if( file_exists( 'functions.php' ) ){
		include('functions.php');	
		$instant = new HelperClass();
		$instant->insertEvent();	
	}
?>
<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Zoom Template</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/styles.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		 <style type="text/css">
            html, body, #map-canvas  {
			  margin: 0;
			  padding: 0;
			  height: 100%;
			}

			#map-canvas {
			  width:500px;
			  height:480px;
			}
        </style>
        <script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/jquery-ui.js"></script>
        <script type='text/javascript' src="http://maps.googleapis.com/maps/api/js?sensor=false&extension=.js&output=embed"></script>
        <script type='text/javascript' src="assets/js/maps.js"></script>
	</head>
	<body>
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo $home_url; ?>">Zoom Template</a>
				</div>
				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a class="pull-right" href='<?php echo $home_url; ?>admin.php?status=loggedout'>Logout</a>
						<span class="pull-right loggedin"><?php echo ucfirst('Admin'); ?> logged In</span></li>
					</ul>
				</div>
			</div>
		</nav>
		<section class="event-section">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2>Event List</h2>
						<div class="panel-wrapper common-form">
						<?php $datalist = $instant->dataFetch(); 

							while( $rows = mysql_fetch_assoc( $datalist ) ) : 
								?>
								<div class="panel-head">
									<?php echo $rows['event_title']; ?>  / <?php echo $instant->fetchCountry( $rows['event_country'] ); ?>  <?php echo $rows['event_start']; ?> /  <?php echo $rows['event_end']; ?>
									<span class="show-hide-btn btn">Show</span>
								</div>	
								<div class="panel-list" style="display: none;">
								<div class="grouplist">
									<table class="table-bordered table">
										<tbody>
											<tr class="row">
												<th class="col-md-3">Event Title</th>
												<td class="col-md-9"><?php echo $rows['event_title']; ?></td>
											</tr>
											<tr class="row">
												<th class="col-md-3">Event Category</th>
												<td class="col-md-9"><?php echo $rows['event_cat']; ?></td>
											</tr>
											<tr class="row">
												<th class="col-md-3">Event Description</th>
												<td class="col-md-9"><?php echo $rows['event_desc']; ?></td>
											</tr>
											<tr class="row">
												<th class="col-md-3">Event Address</th>
												<td class="col-md-9"><?php echo $rows['event_address']; ?></td>
											</tr>
											<tr class="row">
												<th class="col-md-3">Event Zipcode</th>
												<td class="col-md-9"><?php echo $rows['event_zipcode']; ?></td>
											</tr>
											<tr class="row">
												<th class="col-md-3">Country</th>
												<td class="col-md-9"><?php echo $instant->fetchCountry( $rows['event_country'] ); ?></td>
											</tr>

											<tr class="row">
												<th class="col-md-3">Latitude</th>
												<td class="col-md-9"><?php echo $rows['event_lat']; ?></td>
											</tr>

											<tr class="row">
												<th class="col-md-3">Longitude</th>
												<td class="col-md-9"><?php echo $rows['event_long']; ?></td>
											</tr>
											<tr class="row">
												<th class="col-md-3">Event Start date</th>
												<td class="col-md-9"><?php echo $rows['event_start']; ?></td>
											</tr>
											<tr class="row">
												<th class="col-md-3">Event End date</th>
												<td class="col-md-9"><?php echo $rows['event_end']; ?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						<?php
							endwhile;
						?>
						</div>
						<div class="wrap-add-event mt-20">
							<div class="panel-head">
								Add Event Form
							</div>
							<div class="panel-form">
								<form class="form-horizontal" method="POST" action="#">
							      <div class="form-group">
							        <label for="inputTitle1" class="col-sm-3 control-label text-left">Event Title</label>
							        <div class="col-sm-9">
							          <input type="text" class="form-control" id="inputTitle1" name="event_title" placeholder="Please Enter Title" required="required">
							        </div>
							      </div>
							      <div class="form-group">
							        <label for="inputCat1" class="col-sm-3 control-label text-left">Event Category</label>
							        <div class="col-sm-9">
							          <select name="event_cat" class="form-control" id="inputCat1" required="required">
							          	<option>Choose Category</option>
							          	<?php  
							          		$category = $instant->fetchCategory(); 
							          		foreach( $category as $key_category=> $val_category ){ ?>
							          			<option value="<?php echo $key_category; ?>"><?php echo $val_category; ?></option>
							          	<?php	}
							          	?>
							          </select>
							        </div>
							      </div>
							      <div class="form-group">
							        <label for="inputDesc1" class="col-sm-3 control-label text-left">Event Description</label>
							        <div class="col-sm-9">
							          <textarea class="form-control" name="event_desc" id="inputDesc1" cols="5" rows="4" placeholder="Please Enter Deccription" required="required"></textarea>
							        </div>
							      </div>
							      <div class="form-group">
							        <label for="inputAddr1" class="col-sm-3 control-label text-left">Event Address</label>
							        <div class="col-sm-9">
							          <textarea name="event_add" class="form-control" id="inputAddr1" cols="5" rows="4" placeholder="Please Enter Address" required="required"></textarea>
							        </div>
							      </div>
							      <div class="form-group">
							        <label for="inputzip1" class="col-sm-3 control-label text-left">Event Zipcode</label>
							        <div class="col-sm-9">
							          <input type="number" class="form-control" pattern="[0-9]*" maxlength="5" min="0" name="event_zipcode" id="inputzip1" placeholder="Please Enter Zip" required="required" />
							        </div>
							      </div>
							       <div class="form-group">
							        <label for="inputcountry1" class="col-sm-3 control-label text-left">Event Country</label>
							        <div class="col-sm-9">
							          <select name="event_country" class="form-control" required="required">
							          	<option>Choose Option</option>
							          	<?php  
							          		$country = $instant->fetchCountry(); 
							          		foreach( $country as $key_country=> $val_country ){ ?>
							          			<option value="<?php echo $key_country; ?>"><?php echo $val_country; ?></option>
							          	<?php	}
							          	?>

							          </select>
							        </div>
							      </div>
							      <div class="form-group">
							        <label for="inputlat" class="col-sm-3 control-label text-left">Event Latitude/ Longitude</label>
							        <div class="col-sm-3">
							          <input type="text" class="form-control lat" id="inputlat" name="event_lat" placeholder="Choose Latitude" required="required">
							        </div>
							        <div class="col-sm-3">
							          <input type="text" class="form-control long" id="inputlong" name="event_long" placeholder="Choose Longitude" required="required">
							        </div>
							        <div class="col-sm-3">
							          <a href="#myMapModal" class="btn choosemarker" data-toggle="modal">Select Location</a>
							        </div>
							      </div>
							      <div class="form-group">
							        <label for="inputstartdate1" class="col-sm-3 control-label text-left">Event Start Date</label>
							        <div class="col-sm-9">
							          <input type="text" class="form-control datepicker" id="inputstartdate1" name="event_start" placeholder="Please Enter Start Date" required="required">
							        </div>
							      </div>
							      <div class="form-group">
							        <label for="inputenddate1" class="col-sm-3 control-label text-left">Event End Date</label>
							        <div class="col-sm-9">
							          <input type="text" class="form-control datepicker" id="inputenddate1" name="event_end" placeholder="Please Enter End Date" required="required">
							        </div>
							      </div>
							      <div class="form-group">
							        <label for="inputurl1" class="col-sm-3 control-label text-left">Event URL</label>
							        <div class="col-sm-9">
							          <input type="text" class="form-control" id="inputurl1" name="event_url" placeholder="Please Enter URL" required="required">
							        </div>
							      </div>
							      <div class="form-group">
							        <div class="col-sm-offset-3 col-sm-9">
							          <button type="submit" class="btn btn-default">Add Event</button>
							        </div>
							      </div>
							      <div class="form-group">
							        <div class="col-sm-offset-2 col-sm-10">
							        </div>
							      </div>
							    </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<div class="modal fade" id="myMapModal">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		                 <h4 class="modal-title">Modal title</h4>

		            </div>
		            <div class="modal-body">
		                <div class="container">
		                    <div class="row">
		                        <div id="map-canvas" class=""></div>
		                    </div>
		                </div>
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		            </div>
		        </div>
		        <!-- /.modal-content -->
		    </div>
		    <!-- /.modal-dialog -->
	    </div>
	    <!-- /.modal -->
		<script src="assets/js/bootstrap.min.js"></script>
		<script>
			jQuery(document).ready(function($) {
				$( '#inputstartdate1,#inputenddate1').datepicker();
				$('.show-hide-btn').on('click', function(){
					$clickent = $(this);
			       	$clickent.parent().next().toggle( function(){
			       		if( $clickent.hasClass('bthide') ){
			       			$clickent.text('Show');
			       			$clickent.removeClass('bthide');
			       		}else{
			       			$clickent.text('Hide');
			       			$clickent.addClass('bthide');
			       		}
			       	});
			    });
			    $('#myMapModal').on('shown.bs.modal', function(){
			    	resizingMap();
			    });
        });
		</script>
	</body>
</html>